# Hardware

ESP32-LyraT (check [Mouser](https://www.mouser.com/ProductDetail/Espressif-Systems/ESP32-LyraT?qs=MLItCLRbWsxPzPCja546ZA%3D%3D) or [Digikey](https://www.digikey.com/product-detail/en/espressif-systems/ESP32-LYRAT/1904-1031-ND/9381704))

* Getting started guide: https://docs.espressif.com/projects/esp-adf/en/latest/get-started/get-started-esp32-lyrat.html
* Learn more about the ESP32: http://esp32.net/
* Fair warning: this demo probably requires the external 4 MB PSRAM that exists on the ESP32-WROVER, some other ESP32 dev kits use the smaller WROOM module which doesn't have it. You may be able to squeeze it onto the other dev kits by turning off some stuff (e.g. Bluetooth classic which isn't necessary, I just love Bluetooth classic so I threw it in there)

# Software

Based on JoaoLopesF's example repositories. See those readmes for more background on these projects:

* https://github.com/JoaoLopesF/Esp-Mobile-Apps-Esp32
* https://github.com/JoaoLopesF/Esp-Mobile-Apps-iOS
* https://github.com/JoaoLopesF/Esp-Mobile-Apps-Android

Our modifications:

- ESP32
  - Updated for esp-idf version 4.0, newer versions may work as well
  - Added support for OTA updates (check every 30 seconds while connected to WiFi)
  - Mashed up this project with the A2DP sink demo from esp-idf examples for some reason (you can pair up and stream, but it won't be audible because it isn't set up for the codec on the ESP32-LyraT)
  - Demo added LED commands to control one of the lights on the board (clone/checkout `-b led`)
- Android
  - Updated to work with AndroidX and Android API version 29
  - Added support for OTA update QR code
  - Demo added an LED button to send the board the LED command (clone/checkout `-b led`)
- iOS
  - Updated some Swift code
  - Added support for OTA update QR code
  - Demo added an LED button to send the board the LED command (clone/checkout `-b led`)

# Build and run the projects locally

## ESP32

See [espressif's instructions to install esp-idf version 4.0](https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html) or newer (I tested it a while back with a 4.2 development version, but can't make any guarantees)

or use this Docker image: registry.gitlab.com/radiosound/idf:v4-0

Clone the ESP32 firmware project:

```sh
git clone https://gitlab.com/radiosound/derby-devops-spring-2020/esp32
cd esp32
```

If you're using the ESP32-LyraT board, I highly recommend putting jumpers on the IO0, IO2, and EN headers near the USB UART port. This allows the flasher to automatically reset the ESP32 into the bootloader and makes flashing easy peasy. Otherwise, you need to hold the boot button and reset it into that mode manually every time you flash

You will need a few environment variables:

```sh
export ESPBAUD=2000000
export WIFI_SSID="FBI Party Van"
export WIFI_PASSWORD="Great Password Here"
```

Before you build, change main/Kconfig.projbuild to set the default production firmware update URL for your specific environment, otherwise your ESP32 is going to keep downloading whatever was last deployed to the Derby DevOps demo.

Or you can just change it in the esp-idf's menu config:

 ```sh
idf.py menuconfig
 ```

Build it, and they will come:

```sh
idf.py build flash monitor
```

Shortcut to running the Docker image in case trying to install esp-idf natively doesn't work out:

```sh
alias idf.py='docker run --rm -it --env WIFI_SSID="FBI Party Van" --env WIFI_PASSWORD="Great Password Here" --env ESPBAUD=2000000 -v $PWD:/project -w /project --device=/dev/ttyUSB0 registry.gitlab.com/radiosound/idf:v4-0 idf.py'
```

Substitute for another serial port if you have some other device using ttyUSB0

## iOS

Xcode 13.3.1 on macOS Catalina 10.15.3

If you don't have Xcode yet, this will install it from the Mac App Store:

```sh
mas install 497799835
```

Clone the project:

```sh
git clone https://gitlab.com/radiosound/derby-devops-spring-2020/ios
cd ios
open EspApp/EspApp.xcodeproj
```

Change the team provisioning profile in the "Signing & Capabilities" tab of the project settings to your team. (Click the "EspApp" at the top of the project hierarchy in the left sidebar to get to the project settings)

## Android

Android Studio 3.6.1, Android API 29

```sh
git clone https://gitlab.com/radiosound/derby-devops-spring-2020/android
```

Open Android Studio and open the EspApp subdirectory from here as the project

# Set up the ESP32 OTA updates

OTA updates for this project lean heavily on GitLab Auto DevOps.

They are delivered by packaging the ESP32 firmware update into a Docker image and deploying that to a Kubernetes cluster. They are packaged along with a version.txt file and a simple web page that just shows a QR code.

Review apps create a temporary URL at another subdomain, and the mobile app can scan the QR code to tell the ESP32 to which it's connected that you mean to install that test version on the ESP32.

## Kubernetes

Set up a Kubernetes cluster or integrate an existing one into your GitLab group or instance.

https://docs.gitlab.com/ee/user/project/clusters/index.html#adding-and-removing-clusters

You should set up cert-manager if you don't already have it. GitLab's Kubernetes cluster management page has an easy button to click to install it if you don't have it.

## ESP32 project settings in GitLab

Settings -> CI/CD -> Variables

* KUBE_INGRESS_BASE_DOMAIN: set to your specific base domain, e.g. **bleenky.apps.radiosound.com**
* PRODUCTION_ADDITIONAL_HOSTS: set this to the same as you set the above, for the "production" version
* WIFI_SSID: set to the name of your WiFi network. Must be a 2.4ghz network
* WIFI_PASSWORD: the password for that WiFi network
* Some GitLab Auto DevOps stages do not know what to do with this kind of code yet, so, set these variables in order to avoid pipeline failures:
  * CODE_QUALITY_DISABLED: set to **true**
  * TEST_DISABLED: set to **true**
  * PERFORMANCE_DISABLED: set to **true**

## ESP32 Firmware

```sh
idf.py menuconfig
```

Make sure to set the production URL firmware upgrade base in the config (probably the same as your KUBE_INGRESS_BASE_DOMAIN). All temporary URLs must contain this URL as well.

If you plan to use a different SSL certificate, make sure to change out the cert pem in the ESP32's firmware. (Currently set to a Let's Encrypt X3 which works with the certificates obtained by cert-manager)

The generated QR code in web/htdocs/index.html is set to just echo the current URL but change `https` to `bleenky`. The apps are set to recognize that custom URL scheme. If you want to change that, swap out the word `bleenky` - make sure to change it in the Android and iOS app configurations as well.

## Android

So that the app can recognize the QR code for setting the firmware upgrade location: Modify the URL in EspApp/app/src/main/AndroidManifest.xml to match the KUBE_INGRESS_BASE_DOMAIN you set for the ESP32 firmware deployments. Make sure to leave the * in there, or it won't match the temporary test environments. (These will wind up looking like `110-review-4-branch-name-aksdjfka.$KUBE_INGRESS_BASE_DOMAIN`)

If you changed the URL scheme from bleenky:// to something else, this is also where you'd change that.

## iOS

Modify the identifier and/or scheme in EspApp/EspApp/Info.plist or in the Xcode project settings (click the topmost EspApp in the project folder to bring up the Xcode project settings, then go to the Info tab)

I tend to call the identifier the reverse of the domain name, although this may not be necessary.

Change the hostname it's looking for in AppDelegate.swift line 86 to match your KUBE_INGRESS_BASE_DOMAIN

# Set up mobile app CI and local deployments

The local deployment jobs require a GitLab runner that is on the same network as the mobile device, or is connected to it via USB cable.

## iOS

[Install GitLab runner on your Mac](https://docs.gitlab.com/runner/install/osx.html)

[Register the runner](https://docs.gitlab.com/runner/register/index.html#macos) to your group or project (using the shell executor), and make sure to specify the tag `mac` when it asks. The iOS project's CI is set to use only runners with the `mac` tag. If you already have a runner registered on your Mac with a different tag, either add the tag to it in the runners administration page, or change your .gitlab-ci.yml file accordingly.

Register devices in Xcode's Devices & Simulators window (shift+cmd+2)

Plug them in via USB cable to pair up, and check "Connect via network". Once the globe icon shows up, you're good to go. If it isn't showing up, you may need to restart the device, or your router is blocking devices from talking to each other. You can continue via USB if it's convenient to do so.

You need to do this before building in CI, as Xcode will sign only with the keys for devices registered here.

### Local deployment via ios-deploy

Install [ios-deploy](https://github.com/ios-control/ios-deploy):

```sh
brew install ios-deploy
```

Check that your iOS devices are listed:

```sh
ios-deploy -C
```

If it works, you'll see the ID and name of the device

```
[....] Waiting for iOS device to be connected
[....] Using 29a072ba3b9ed26126bb128f4274c2c17f93d482 (N51AP, iPhone 5s (GSM), iphoneos, arm64) a.k.a. 'iPhone 5s silver (RSI Eng)'.
```

Grab the device ID for this one (in this case `29a072ba3b9ed26126bb128f4274c2c17f93d482`) and set it up in your GitLab CI/CD variables:

* DEVICE_ID_IPHONE_5S_SILVER: 29a072ba3b9ed26126bb128f4274c2c17f93d482
* DEVICE_ID_MY_OTHER_PHONE: insert ID here, etc.

Change the .gitlab-ci.yml to reference your devices.

```yaml
review iPhone 6 Plus:
  environment:
    name: iPhone 6 Plus
  variables:
    DEVICE_ID: $DEVICE_ID_IPHONE_6_PLUS
  <<: *review_common
```

^-- Swap out the names and device ID variable names for these four as needed. I wouldn't recommend putting the IDs themselves in the yml file, just reference a GitLab variable.

If you want to keep the testfleet job in the CI file (just a shortcut to deploying to all at once), you'll need to modify that section to reference your devices as well.

The device IDs work whether they are connected to the Mac via USB or WiFi

### TestFlight / App Store

You can drop local deployment and go with fastlane and TestFlight if you don't mind a 10-30 minute wait to deliver internal test builds from CI. Follow GitLab's blog post about [publishing iOS apps to the App Store with GitLab and fastlane](https://about.gitlab.com/blog/2019/03/06/ios-publishing-with-gitlab-and-fastlane/)

## Android

### Keys

Once you've installed adb, you'll need your debug signing key and ADB keys for building the app in CI and deploying to devices locally. You should use the same keys on every development computer and CI runner that you plan to use to build and/or deploy these apps, otherwise you'll get errors when adb attempts to install, such as INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES

* ~/.android/adbkey
* ~/.android/adbkey.pub
* ~/.android/debug.keystore

### Settings -> CI/CD -> Variables

* Add the contents of adbkey.pub to the ADBKEY_PUB variable

* Add the contents of adbkey to the ADBKEY_PRIV variable

* Then get the base64 version of the debug keystore:

  ```sh
  echo -n ~/.android/debug.keystore | base64
  ```

  and put it in the DEBUG_KEYSTORE variable

* Some GitLab Auto DevOps stages do not know what to do with this kind of code yet, so, set these variables in order to avoid pipeline failures:

  * CODE_QUALITY_DISABLED: set to **true**
  * TEST_DISABLED: set to **true**
  * PERFORMANCE_DISABLED: set to **true**

### GitLab Runner

Register a runner with the tag: android-deploy

It can be a Docker runner, or a shell runner if the machine has adb and Android SDK installed

### Wireless adb

**Only do this on a trusted network**

The following script should do the trick to set the device up for adb over the network; substitute for your IP range if necessary:

```sh
# First connect the device via USB to set up tcpip
adb tcpip 5555

# Get the IP
export ANDROID_DEVICE_IP_ADDRESS=$(adb shell ifconfig wlan0 | awk '{print $2}' | grep 192.168.1 | awk -F ':' '{print $2}')

# Connect to the device
adb connect $ANDROID_DEVICE_IP_ADDRESS:5555
```

You can now disconnect the cable.

If it complains about multiple devices connected:

```sh
adb devices
adb -s <ID> tcpip 5555
```

where `<ID>` is the desired device.

Settings -> CI/CD -> Variables

* ANDROID_IP_YOUR_PHONE: the IP address that it found in the above command (echo $ANDROID_DEVICE_IP_ADDRESS)

Change the local deploy jobs

e.g.

```sh
local deploy z3:
  <<: *local_deploy_common
  variables:
    ANDROID_SERIAL: $ANDROID_IP_Z3:5555
  environment:
    name: Z3
```

change Z3 to your phone's name, and $ANDROID_IP_Z3:5555 to $ANDROID_IP_YOUR_PHONE:5555



### Wired adb

If you want to do it wired instead, you will probably need to remove all `adb connect` references from the GitLab CI script

```sh
adb devices
```

Get the serial number of the desired device

Settings -> CI/CD -> Variables

* ANDROID_SERIAL_YOUR_PHONE: set to one of the serial numbers listed above

Change the local deploy jobs

e.g.

```sh
local deploy z3:
  <<: *local_deploy_common
  variables:
    ANDROID_SERIAL: $ANDROID_IP_Z3:5555
  environment:
    name: Z3
```

change Z3 to your phone's name, and $ANDROID_IP_Z3:5555 to $ANDROID_SERIAL_YOUR_PHONE

### Web deployments

The current CI file leans heavily on the GitLab Auto DevOps template, and packages the .apk file to a website deployed to your Kubernetes cluster. You will see these get deployed to your Operations -> Environments page in GitLab, and you can either open them on your phone and hit the install button, or open them on your computer from that page and scan the QR code with your phone. This can be used if you are outside of the network and need to update your phone with a new version of the app.

### Play Store

You may be able to borrow some steps from GitLab's blog post: https://about.gitlab.com/blog/2019/01/28/android-publishing-with-gitlab-and-fastlane/

Where you see "Setting up the Docker build environment", just ignore those and continue to use the fantastic image maintained by jangrewe that we're using for the CI build in this project.